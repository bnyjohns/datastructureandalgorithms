﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class Node
    {
        public Node(int data)
        {
            Data = data;
        }
        public int Data { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }
    }

    public class Tree
    {
        public Node Root { get; set; }

        public void Insert(int data)
        {
            if (Root == null)
                Root = new Node(data);
            else
            {
                var current = Root;
                while (true)
                {
                    if(data <= current.Data)
                    {
                        if (current.Left == null)
                        {
                            current.Left = new Node(data);
                            break;
                        }
                        else
                            current = current.Left;
                    }
                    else
                    {
                        if (current.Right == null)
                        {
                            current.Right = new Node(data);
                            break;
                        }
                        else
                            current = current.Right;
                    }
                }
            }
        }

        public int GetHeight(Node root)
        {
            if (root == null)
                return 0;

            if (root.Left == null && root.Right == null)
                return 0;

            return 1 + Math.Max(GetHeight(root.Left), GetHeight(root.Right));
        }
    }
}
