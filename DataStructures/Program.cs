﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree tree = new Tree();
            tree.Insert(5);
            tree.Insert(4);
            tree.Insert(2);
            tree.Insert(3);
            tree.Insert(6);
            tree.Insert(7);

            Console.WriteLine(tree.GetHeight(tree.Root));
        }
    }
}
