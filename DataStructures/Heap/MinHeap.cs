﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DataStructures
{
    public class MinHeap
    {
        List<int> input = null;
        bool heapified = false;
        public MinHeap()
        {
            input = new List<int>();
        }

        public int Count
        {
            get
            {
                return input.Count;
            }
        }

        public MinHeap(int[] input)
        {
            this.input = input.ToList();
        }

        public int Min()
        {
            return input[0];
        }

        public void BuildMinHeap()
        {
            for (int i = input.Count / 2; i >= 0; i--)
            {
                Heapify(i, input.Count);
            }
        }

        private void Heapify(int i, int length)
        {
            var left = i * 2 + 1;
            var right = i * 2 + 2;

            var lowest = i;
            if (left < length && input[left] < input[lowest])
                lowest = left;
            if (right < length && input[right] < input[lowest])
                lowest = right;

            if (lowest != i)
            {
                Swap(lowest, i);
                Heapify(lowest, length);
            }
        }

        private void Swap(int index1, int index2)
        {
            var t = input[index1];
            input[index1] = input[index2];
            input[index2] = t;
        }

        public void Insert(int inp)
        {
            input.Add(inp);
            var index = input.Count - 1;
            var parentIndex = index / 2;

            while (parentIndex >= 0)
            {
                var current = input[index];
                var parent = input[parentIndex];

                if (current < parent)
                {
                    Swap(index, parentIndex);
                    index = parentIndex;
                    parentIndex = index / 2;
                }
                else
                    break;
            }
        }

        public void Delete(int inp)
        {
            var index = input.IndexOf(inp);
            Swap(index, input.Count - 1);
            input.RemoveAt(input.Count - 1);
            Heapify(index, input.Count);
        }
    }


}