﻿using Algorithms.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructures
{
    public class MaxHeap 
    {
        List<int> _input = null;
        
        public MaxHeap(int[] input)
        {
            _input = input.ToList();
            Build();
        }

        public int Max
        {
            get
            {
                return _input[0];
            }
        }

        public int ExtractMax()
        {
            var max = _input[0];
            var length = _input.Count;
            SwapIndexValues(_input, 0, length - 1);            
            _input.RemoveAt(length - 1);
            length--;

            Max_Heapify(_input, 0, length);
            return max;
        }

        public void Insert(int inp)
        {
            _input.Add(inp);
            var index = _input.Count - 1;
            var parentIndex = index / 2;
            while(parentIndex >= 0)
            {
                var current = _input[index];
                var parent = _input[parentIndex];
                if (current > parent)
                {
                    SwapIndexValues(_input, index, parentIndex);
                    index = parentIndex;
                    parentIndex = index / 2;
                }
                else
                    break;
            }

        }

        private void Build()
        {
            for (int i =  _input.Count / 2 - 1; i >= 0; i--)
            {
                Max_Heapify(_input, i, _input.Count);
            }
        }

        private void SwapIndexValues(List<int> list, int a, int b)
        {
            var temp = list[a];
            list[a] = list[b];
            list[b] = temp;
        }

        private void Max_Heapify(List<int> arr, int i, int n)
        {
            int left = 2*i + 1;
            int right = 2*i + 2;
            int largest = i;

            if (left < n && arr[left] > arr[largest])
                largest = left;

            if (right < n && arr[right] > arr[largest])
                largest = right;

            if(largest != i)
            {
                SwapIndexValues(arr, largest, i);
                Max_Heapify(arr, largest, n);
            }
        }        
    }
}
