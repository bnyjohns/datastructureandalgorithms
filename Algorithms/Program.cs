﻿using Algorithms.DynamicProgramming;
using Algorithms.Graphs;
using Algorithms.Searching;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms
{
    class Program
    {
        public static void Main()
        {
            //TravellingSalesman.Start();

            WeightedGraph graph = new WeightedGraph(false);
            graph.Add('w', 'y', 1);
            graph.Add('w', 'x', 6);
            graph.Add('w', 'z', 3);

            graph.Add('y', 'z', 2);
            graph.Add('y', 'x', 4);

            graph.Add('x', 'x', 1);

            Console.WriteLine(graph.ShortestPath('w', 'x'));
        }
    }
}
