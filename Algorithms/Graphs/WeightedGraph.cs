﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algorithms.Graphs
{
    class WeightedGraph
    {
        private bool _directed = false;
        private Dictionary<char, List<Tuple<char,int>>> _graph = null;

        public WeightedGraph(bool directed)
        {
            _directed = directed;
            _graph = new Dictionary<char, List<Tuple<char, int>>>();
        }

        public void Add(char source, char dest, int weight)
        {
            if (_graph.ContainsKey(source))
                _graph[source].Add(new Tuple<char, int>(dest, weight));
            else
                _graph.Add(source, new List<Tuple<char, int>>{ new Tuple<char, int>(dest, weight) });

            if (!_directed)
            {
                if (_graph.ContainsKey(dest))
                    _graph[dest].Add(new Tuple<char, int>(source, weight));
                else
                    _graph.Add(dest, new List<Tuple<char, int>> { new Tuple<char, int>(source, weight) });
            }
        }

        public List<Tuple<char, int>> Edges(char vertex)
        {
            return _graph[vertex];
        }

        public List<char> Vertices
        {
            get
            {
                return _graph.Keys.ToList();
            }
        }

        public int VerticeCount { get { return _graph.Count; } }

        public int ShortestPath(char source, char dest)
        {
            Dictionary<char, int> vertexValues = new Dictionary<char, int>();
            Vertices.ForEach(v =>
            {
                if (v == source)
                    vertexValues.Add(v, 0);
                else
                    vertexValues.Add(v, int.MaxValue);
            });

            Queue<char> queue = new Queue<char>();
            queue.Enqueue(source);
            HashSet<char> visited = new HashSet<char>();
            while (queue.Any())
            {
                var vertex = queue.Dequeue();//Use priority queue so that least vertex value edge dequeues first
                visited.Add(vertex);
                var edges = Edges(vertex);
                var currrentVertexVal = vertexValues[vertex];
                foreach (var edge in edges)
                {
                    var e = edge.Item1;
                    var v = edge.Item2;

                    if (visited.Contains(e))
                        continue;

                    var edgeVertexVal = vertexValues[e];
                    var newEdgeVertexVal = currrentVertexVal + v;
                    vertexValues[e] = Math.Min(edgeVertexVal, newEdgeVertexVal);

                    queue.Enqueue(e);
                }
            }
            return vertexValues[dest];
        }
    }
}
